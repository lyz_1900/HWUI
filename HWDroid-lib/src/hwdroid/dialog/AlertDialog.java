package hwdroid.dialog;

import android.content.Context;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import hwdroid.dialog.DialogInterface.OnCancelListener;
import hwdroid.dialog.DialogInterface.OnDismissListener;
import hwdroid.dialog.DialogInterface.OnShowListener;

public class AlertDialog extends Dialog{
    /**
     * The identifier for the positive button.
     */
    public static final int BUTTON_POSITIVE = -1;

    /**
     * The identifier for the negative button. 
     */
    public static final int BUTTON_NEGATIVE = -2;

    /**
     * The identifier for the neutral button. 
     */
    public static final int BUTTON_NEUTRAL = -3;
    
	public  DropDownDialog mXXDropDownDialog;
	
	public AlertDialog(Context context) {
		this(context, 0);
	}
	
	public AlertDialog(Context context, int attr) {
		super(context, attr);
		initXXDropDownDialog(context);
	}
	
	private void initXXDropDownDialog(Context context) {
		mXXDropDownDialog = new DropDownDialog(context);
	}
	
	@Override
	public void setCancelMessage(Message msg) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setCancelable(boolean flag) {
		mXXDropDownDialog.setCancelable(flag);
	}

	@Override
	public void setCanceledOnTouchOutside(boolean cancel) {
	    mXXDropDownDialog.setCanceledOnTouchOutside(cancel);
	}

	@Override
	public void setDismissMessage(Message msg) {
		// TODO Auto-generated method stub
	}

	@Override
	public void setOnCancelListener(OnCancelListener listener) {
		mXXDropDownDialog.setOnCancelListener(listener);
	}

	@Override
	public void setOnDismissListener(OnDismissListener listener) {
		mXXDropDownDialog.setOnDismissListener(listener);
	}
	
	@Override
	public void setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
	    mXXDropDownDialog.setOnKeyListener(onKeyListener);
	}

	@Override
	public void show() {
		mXXDropDownDialog.showDialog();
	}
    
    public void setTitle(int titleId) {
    	mXXDropDownDialog.setTitle(titleId);
    }
    
    public void setTitle(CharSequence title) {
    	mXXDropDownDialog.setTitle(title);
    }
    
    public void setView(View v) {
    	mXXDropDownDialog.setView(v);
    }
    
    public Button getButton(int whichButton) {
        return mXXDropDownDialog.getButton(whichButton);
    }
    
	@Override
	public void dismiss() {
		mXXDropDownDialog.callDismiss();
	}

	public void setButton(int whichButton, CharSequence text, DialogInterface.OnClickListener listener) {
	    if(whichButton == BUTTON_POSITIVE) {
	        mXXDropDownDialog.setButton1(text, listener);
	    } else if(whichButton == BUTTON_NEGATIVE) {
            mXXDropDownDialog.setButton2(text, listener);
	    }
	}
	
    public void setButton(CharSequence text, DialogInterface.OnClickListener listener) {   	
        mXXDropDownDialog.setButton1(text, listener);
    }
    
    public void setButton2(CharSequence text, DialogInterface.OnClickListener listener) {    
        mXXDropDownDialog.setButton2(text, listener);
    }
    
    public void setButton3(CharSequence text, DialogInterface.OnClickListener listener) {    
        mXXDropDownDialog.setButton3(text, listener);
    }
    
    public View getCustomView() {
    	return null;
    }
    
	@Override
	public Window getWindow() {
		return mXXDropDownDialog.getWindow();
	}
	
    @Override
    public boolean isShowing() {
        return mXXDropDownDialog.isShowing();
    }

    public void setOnShowListener(OnShowListener onShowListener) {
        
    }

    public void cancel() {
        mXXDropDownDialog.cancel();
    }

    public void setMessage(CharSequence message) {
        mXXDropDownDialog.setMessage(message);
    }
	
    public static class Builder {
    	Context mContext;
    	AlertDialog mDialog;

        public Builder(Context context) {
        	mContext = context;
        	mDialog = new AlertDialog(mContext);
        }
            
        public Context getContext() {
            return mContext;
        }

        public Builder setTitle(int titleId) {
        	mDialog.mXXDropDownDialog.setTitle(titleId);
            return this;
        }
        
        public Builder setTitle(CharSequence title) {
        	mDialog.mXXDropDownDialog.setTitle(title);
            return this;
        }
        
        public Builder setCustomTitle(View customTitleView) {
            //TODO: this case is worning. don't set customTitleView to customview!!!!!
            mDialog.mXXDropDownDialog.setView(customTitleView);
            return this;
        }
        
        public Builder setMessage(int messageId) {
        	mDialog.mXXDropDownDialog.setMessage(messageId);
            return this;
        }
        
        public Builder setMessage(CharSequence message) {
        	mDialog.mXXDropDownDialog.setMessage(message);
            return this;
        }
        
        public Builder setPositiveButton(final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton1(android.R.string.ok, listener);
            return this;
        }

        public Builder setPositiveButton(int textId, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton1(textId, listener);
            return this;
        }
        
        public Builder setPositiveButton(CharSequence text, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton1(text,listener);
            return this;
        }
        
        public Builder setNegativeButton(final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton2(android.R.string.cancel, listener);
            return this;
        }
        
        public Builder setNegativeButton(int textId, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton2(textId, listener);
            return this;
        }
        
        public Builder setNegativeButton(CharSequence text, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setButton2(text, listener);
            return this;
        }
        
        public Builder setNeutralButton(final DialogInterface.OnClickListener listener) {
            mDialog.mXXDropDownDialog.setButton3(android.R.string.cancel, listener);
            return this;
        }
        
        public Builder setNeutralButton(int textId, final DialogInterface.OnClickListener listener) {
            mDialog.mXXDropDownDialog.setButton3(textId, listener);
            return this;
        }
        
        public Builder setNeutralButton(CharSequence text, final DialogInterface.OnClickListener listener) {
            mDialog.mXXDropDownDialog.setButton3(text, listener);
            return this;
        }        
        
        public Builder setCancelable(boolean cancelable) {
        	mDialog.mXXDropDownDialog.setCancelable(cancelable);
            return this;
        }
        
        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        	mDialog.setOnCancelListener(onCancelListener);
            return this;
        }
        
        public Builder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
            mDialog.setOnKeyListener(onKeyListener);
            return this;
        }
        
        public Builder setItems(CharSequence[] items, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.addItems(items, listener);
            return this;
        }
        
        public Builder setMultiChoiceItems(CharSequence[] items, boolean[] checkedItems, 
                final DialogInterface.OnMultiChoiceClickListener listener) {
        	mDialog.mXXDropDownDialog.setMultiChoiceItems(items, checkedItems, listener);
            return this;
        }
        
        public Builder setSingleChoiceItems(CharSequence[] items, int checkedItem, final DialogInterface.OnClickListener listener) {
        	mDialog.mXXDropDownDialog.setSingleChoiceItems(items, checkedItem, listener);
        	return this;
        } 

        public Builder setView(View v) {
        	mDialog.setView(v);
            return this;
        }

        public AlertDialog create() {
            return mDialog;
        }

        public Builder setItems(int itemsId,
                hwdroid.dialog.DialogInterface.OnClickListener listener) {
            // TODO Auto-generated method stub
            CharSequence[] items = this.mContext.getResources().getTextArray(itemsId);
            mDialog.mXXDropDownDialog.addItems(items, listener);
            return this;
        }

        public Builder setSingleChoiceItems(int itemsId, int checkedItem,
                hwdroid.dialog.DialogInterface.OnClickListener listener) {
            // TODO Auto-generated method stub
            CharSequence[] items = this.mContext.getResources().getTextArray(itemsId);
            mDialog.mXXDropDownDialog.setSingleChoiceItems(items, checkedItem, listener);
            return null;
        }

        public Builder setOnDismissListener(OnDismissListener listener) {
            mDialog.mXXDropDownDialog.setOnDismissListener(listener);
            return this;
        }
        
    }    
}
