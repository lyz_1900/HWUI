package com.hw.droid.hwcatalog;

import hwdroid.app.HWActivity;
import hwdroid.widget.ActionBar.ActionBarView.OnTitle2ItemClick;
import hwdroid.widget.FooterBar.FooterBarButton;
import hwdroid.widget.FooterBar.FooterBarItem;
import hwdroid.widget.FooterBar.FooterBarType.OnFooterItemClick;
import hwdroid.widget.FooterBar.FooterBarView;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FooterBarButtonActivity extends HWActivity{

	private int mButtonItemId = 0;
    
    private Toast mToast;

    private FooterBarView mFooterBarButton;
    
    LinearLayout mMainView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setActivityContentView(R.layout.menu_footer);
		this.showBackKey(true);
		
		
		mMainView = (LinearLayout)this.findViewById(R.id.main);
		mToast = new Toast(this);
		
		this.setTitle2("click me", new OnTitle2ItemClick(){

			@Override
			public void onTitle2ItemClick() {
				showToast("you click title ");
			}});
		
		
		mFooterBarButton = new FooterBarButton(this);
		mFooterBarButton.setOnFooterItemClick(new OnFooterItemClick() {
			@Override
			public void onFooterItemClick(View view, int id) {
				showToast("Click FooterBarButton Item. Id = " + id);
			}});
		
		mFooterBarButton.addItem(mButtonItemId++, 
    			this.getResources().getText(R.string.menu_new));
		mFooterBarButton.addItem(mButtonItemId++, 
    			this.getResources().getText(R.string.menu_new)); 	
		mFooterBarButton.updateItems();	
		
		getFooterBarImpl().addView(mFooterBarButton);	
		getFooterBarImpl().setVisibility(View.VISIBLE);

		initAction2();
		initAction3();
	}
	
	private Button createButton(String title) {
		Button btn = new Button(this);
		btn.setText(title);
		
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		btn.setLayoutParams(lp);
		mMainView.addView(btn);
		
		return btn;
	}
	
	private void initAction2() {

		createButton("addFooterBarItem").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				addFooterBarItem();
			}});
		
		createButton("removeFooterBarItem").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				removeFooterBarItem();
			}});		

	}
	
	private void initAction3() {
		createButton("setItemEnable").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = 0;
				itemId = mButtonItemId -1;
				
				FooterBarItem item = mFooterBarButton.getItem(itemId);
				if(item != null) {
					mFooterBarButton.setItemEnable(itemId, !item.isItemEnabled());
				}
			}});
		
		createButton("setItemTextColor").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = mButtonItemId -1;
				
				mFooterBarButton.setItemTextColor(itemId, Color.YELLOW);
			}});	

		createButton("setItemTextSize").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = mButtonItemId -1;
				
				mFooterBarButton.setItemTextSize(itemId, 40);
			}});	
		
		createButton("setItemBackgroundColor").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId  = mButtonItemId -1;
				
				mFooterBarButton.setItemBackgroundColor(itemId, Color.RED);
			}});
	}
		
    private void addFooterBarItem() {
		FooterBarItem item = mFooterBarButton.addItem(mButtonItemId++, 
    			this.getResources().getText(R.string.menu_new));
    	
		if(item == null) {
			mButtonItemId--;
			showToast("don't add!!");
		} else {
	    	mFooterBarButton.updateItems();
		}
    }

    private void removeFooterBarItem() {
    	mFooterBarButton.removeItem(--mButtonItemId);	
    	mFooterBarButton.updateItems();
    }
	
    private void showToast(CharSequence text) {
    	TextView t = new TextView(this);
    	t.setBackgroundColor(Color.RED);
    	t.setText(text);
    	t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
}
