package com.hw.droid.hwcatalog;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hwdroid.app.HWActivity;
import hwdroid.widget.ItemAdapter;
import hwdroid.widget.ActionBar.ActionBarView;
import hwdroid.widget.ActionBar.ActionBarView.OnBackKeyItemClick;
import hwdroid.widget.item.Item;
import hwdroid.widget.item.TextItem;
import hwdroid.widget.searchview.SearchView;
import hwdroid.widget.searchview.SearchView.SearchViewListener;

import java.util.ArrayList;
import java.util.List;

public class SearchView3Activity  extends HWActivity {
    private ActionBarView mActionBarView;
    
    private Toast mToast;
    
    LinearLayout mLayout;
    List<Item> mItems;
    ItemAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.activity_searchview_3);
        mActionBarView = (ActionBarView) findViewById(R.id.actionbarview);
        mActionBarView.showBackKey(true, new OnBackKeyItemClick() {
            @Override
            public void onBackKeyItemClick() {
                SearchView3Activity.this.finish();
            }
        });
        
        mActionBarView.setTitle("SearchView Activity");
        
        mToast = new Toast(this);
        mLayout = (LinearLayout)findViewById(R.id.layout); 
        mItems = new ArrayList<Item>(); 
        mAdapter = new ItemAdapter(this, mItems);
        
//        for(int i = 0; i < 30; i++) {
//            mAdapter.add(new TextItem("search item."));
//        } 
         
        //show search view
        long start = System.currentTimeMillis();
        SearchView searchview = new SearchView(this);
        searchview.setSearchAdapter(mAdapter);
        
        Log.i("yingchn", "consume time : " + (System.currentTimeMillis() - start));
        searchview.setAnchorView(mActionBarView);
        searchview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(SearchView3Activity.this, SearchViewActivity.class));
            }
        });
        searchview.setSearchViewListener(new SearchViewListener(){

            @Override
            public void startOutAnimation(int time) {
                //mActionBarView.setVisibility(View.VISIBLE);
            }

            @Override
            public void startInAnimation(int time) {
                //mActionBarView.setVisibility(View.GONE);
            }

            @Override
            public void doTextChanged(CharSequence s) {
                
                mAdapter.clear();
                
                if(s != null && s.length() > 0) {
                    
                    for(int i = 0; i < 30; i++) {
                        mAdapter.add(new TextItem(s.toString()));
                    }
                   
                }
                
                mAdapter.notifyDataSetChanged();
                
            }});
        
        mLayout.addView(searchview);
    }
    
    private void showToast(CharSequence text) {
        TextView t = new TextView(this);
        t.setBackgroundColor(Color.RED);
        t.setText(text);
        t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
}