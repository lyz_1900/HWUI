package com.hw.droid.hwcatalog;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import hwdroid.app.HWActivity;
import hwdroid.widget.FooterBar.FooterBarItem;
import hwdroid.widget.FooterBar.FooterBarMenu;
import hwdroid.widget.FooterBar.FooterBarType.OnFooterItemClick;
import hwdroid.widget.FooterBar.FooterBarView;

public class FooterBarMenuActivity extends HWActivity{

	private int mMenuItemId = 0;
    
    private Toast mToast;

    private FooterBarView mFooterBarMenu;
    
    boolean mButtonType = false;
    LinearLayout mMainView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setActivityContentView(R.layout.menu_footer);
		this.showBackKey(true);
		
		mMainView = (LinearLayout)this.findViewById(R.id.main);
		mToast = new Toast(this);
		
		mFooterBarMenu = new FooterBarMenu(this);
		
		for(mMenuItemId = 0; mMenuItemId < 8; mMenuItemId++) {
		
			mFooterBarMenu.addItem(mMenuItemId, 
					this.getResources().getText(R.string.menu_new), 
	    			this.getResources().getDrawable(R.drawable.hw_footerbar_item_more_icon_normal));
			
			if(mMenuItemId ==6) {
			    mFooterBarMenu.setItemEnable(mMenuItemId, false);
			}
		}
		
		mFooterBarMenu.setPrimaryItemCount(2);
		
		mFooterBarMenu.updateItems();
		
		getFooterBarImpl().addView(mFooterBarMenu);	
		getFooterBarImpl().setVisibility(View.VISIBLE);

		mFooterBarMenu.setOnFooterItemClick(new OnFooterItemClick() {
			@Override
			public void onFooterItemClick(View view, int id) {
				showToast("Click FooterBarMenu Item. Id = " + id);
			}});
		
		initAction2();
		initAction3();
	}
	
	private Button createButton(String title) {
		Button btn = new Button(this);
		btn.setText(title);
		
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		btn.setLayoutParams(lp);
		mMainView.addView(btn);
		
		return btn;
	}
	
	private void initAction2() {

		createButton("addFooterBarItem").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				addFooterBarItem();
			}});
		
		createButton("removeFooterBarItem").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				removeFooterBarItem();
			}});		

	}
	
	private void initAction3() {
		createButton("setItemEnable").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = 0;
				itemId = 1;
				
				FooterBarItem item = mFooterBarMenu.getItem(itemId);
				if(item != null) {
					mFooterBarMenu.setItemEnable(itemId, !item.isItemEnabled());
				}
			}});
		
		createButton("setItemTextColor").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = 1;
				
				mFooterBarMenu.setItemTextColor(itemId, Color.YELLOW);
			}});	

		createButton("setItemTextSize").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId = 1;
				
				mFooterBarMenu.setItemTextSize(itemId, 40);
			}});	
		
		createButton("setItemBackgroundColor").setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				int itemId  = 1;
				
				mFooterBarMenu.setItemBackgroundColor(itemId, Color.RED);
			}});
	}
		
    private void addFooterBarItem() {
		FooterBarItem item = mFooterBarMenu.addItem(mMenuItemId++, 
    			this.getResources().getText(R.string.menu_new), 
    			this.getResources().getDrawable(R.drawable.hw_footerbar_item_more_icon_normal));
    	
		if(item == null) {
			mMenuItemId--;
			showToast("don't add!!");
		} else {
	    	mFooterBarMenu.updateItems();
		}
    }

    private void removeFooterBarItem() {
    	mFooterBarMenu.removeItem(--mMenuItemId);	
    	mFooterBarMenu.updateItems();
    }
	
    private void showToast(CharSequence text) {
    	TextView t = new TextView(this);
    	t.setBackgroundColor(Color.RED);
    	t.setText(text);
    	t.setTextColor(Color.BLACK);
        mToast.setView(t);   
        mToast.setDuration(Toast.LENGTH_SHORT);
        mToast.setGravity(Gravity.TOP, 0, 0);
        mToast.show();
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
