package com.hw.droid.hwcatalog.indicator;

import hwdroid.app.BaseFragmentActivity;
import hwdroid.widget.indicator.HWTabPageIndicator;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.hw.droid.hwcatalog.R;


public class TabIndicatorActivity extends BaseFragmentActivity/*extends FragmentActivity*/ {
    private static final String[] CONTENT = new String[] { "A", "B", "C", "D" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.simple_tabs);
        
        getFooterBarImpl().setVisibility(View.GONE);
        
        FragmentPagerAdapter adapter = new SampleFragmentPagerAdapter(this.getSupportFragmentManager());

        ViewPager pager = (ViewPager)findViewById(R.id.pager);
        pager.setAdapter(adapter);

        HWTabPageIndicator indicator = new HWTabPageIndicator(this);
        indicator.setViewPager(pager);
        if(getActionBarView() != null) {
            getActionBarView().setTabPageIndicator(indicator);
        }
    }

    class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
          return CONTENT.length;
        }
    }
}
